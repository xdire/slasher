﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	private Transform player;
	private Vector3 playerpos;
	private Vector3 newPosition;
	private int screenWidth;
	private int screenHeight;

	private float posz;
	private float movingDelta = 2f;

	private Camera mainCamera;
	
	// Use this for initialization
	void Start () {

		screenWidth = Screen.width;
		screenHeight = Screen.height;

		mainCamera = Camera.main;
		this.posz = mainCamera.transform.position.z;

		player = GameObject.Find ("female-char").transform;

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		float x = 0.0f;
		float delta = 0.02f;
		float posx = Camera.main.transform.position.x;
		float posy = Camera.main.transform.position.y;

		this.playerpos = player.position;
		x = player.position.x;

		if (x > (posx + delta))
				{
				newPosition = new Vector3(x+delta, posy, posz);
				playerpos.z = this.transform.position.z;
				transform.position = Vector3.Lerp(mainCamera.transform.position, newPosition, movingDelta*Time.fixedDeltaTime );
				}

		//Debug.Log ("current PLAYERPOS  = " + playerpos.ToString () + " SW = " + screenWidth.ToString () + " POSx = " + posx.ToString () + "X = " +x.ToString() );
	}
}
