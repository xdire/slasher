﻿using UnityEngine;
using System.Collections;

public class FloatArray {
	
	public float mousex, mousey, magnitude, angle;
	
	public FloatArray (float fax, float fay, float fam, float faa){

		this.mousex = fax;
		this.mousey = fay;
		this.magnitude = fam;
		this.angle = faa;
	}
}
