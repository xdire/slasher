﻿using UnityEngine;
using System.Collections;

public class CursorMove : MonoBehaviour {
	public float touchDistance = 2.0f;
	public GameObject gameCursorObj;
	public Transform gameCursor;

	public ParticleSystem cursorTrail;

	void Start () {

		gameCursorObj = GameObject.Find ("gameCursor");
		gameCursor = GameObject.Find ("gameCursor").transform;

	}
	
	void Update () {

		Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		Vector3 posNewPos = mouseRay.GetPoint(touchDistance);
		
		if (Input.GetMouseButton(0)) {
			cursorTrail = gameCursorObj.GetComponentInChildren<ParticleSystem>();
			cursorTrail.Play(true);
			gameCursorObj.transform.position = posNewPos;

			if(gameCursorObj == null)
			{
				gameCursorObj = Instantiate (gameCursor, posNewPos, Quaternion.identity) as GameObject;
	
				gameCursorObj.transform.position = posNewPos;

				//Debug.Log(" GOVNO: ");
			}
			//Debug.Log(" GAMEOBJECT " + gameCursorObj.ToString());
			//Debug.Log(" POSITION: " + gameCursorObj.transform.position.ToString() );
		}

		if (Input.GetMouseButtonUp(0)) {
			
			if(gameCursorObj)
			{
				cursorTrail = gameCursorObj.GetComponentInChildren<ParticleSystem>();
				cursorTrail.Stop(true);

				Color acolor = gameCursorObj.renderer.material.color;
				acolor.a = 0.5f;
				gameCursorObj.renderer.material.color = acolor;
			}

		}

	}
}
