﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	private Animator animator_;
	
	void Start () {
		Animator[] animators = GetComponentsInChildren<Animator>();
		animator_ = animators[0];
	}

	public void Kill () {
		Debug.Log("Destroy!");
		animator_.SetBool("Destroy", true);
		Destroy(gameObject, animation.clip.length);
	}
}
